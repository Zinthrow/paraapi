from django.shortcuts import render

from django.http import HttpResponse
from django.conf.urls import url

from .models import Entry, Batch, Filter, Panel
from rest_framework import viewsets, permissions, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializer import EntrySerializer, BatchSerializer, FilterSerializer, PanelSerializer, ConfigSerializer
# Create your views here.
 
class ConfigViewSet(viewsets.ModelViewSet):

    queryset = Entry.objects.all()
    serializer_class = ConfigSerializer
    search_fields = ['name', 'batch_id__name']
    filter_backends = (filters.SearchFilter,)
    #permission_classes = [permissions.IsAuthenticated]


class EntryViewSet(viewsets.ModelViewSet):

    queryset = Entry.objects.all()
    serializer_class = EntrySerializer
    search_fields = ['name', 'batch_id__name']
    filter_backends = (filters.SearchFilter,)
    #permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['GET'], url_path='config', url_name='config')
    def config(self, request, *args, **kwargs):
        #self.serializer_class = ConfigSerializer(self.get_queryset(), many=True)
        return Response(ConfigSerializer(self.get_queryset(), many=True).data)


class BatchViewSet(viewsets.ModelViewSet):
    queryset = Batch.objects.all()
    serializer_class = BatchSerializer
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    #permission_classes = [permissions.IsAuthenticated]


class FilterViewSet(viewsets.ModelViewSet):
    queryset = Filter.objects.all()
    serializer_class = FilterSerializer
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    #permission_classes = [permissions.IsAuthenticated]


class PanelViewSet(viewsets.ModelViewSet):
    queryset = Panel.objects.all()
    serializer_class = PanelSerializer
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    #permission_classes = [permissions.IsAuthenticated]
