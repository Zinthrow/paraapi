from django.db import models
from django.utils import timezone

# Create your models here. on_delete=models.SET_NULL


class Filter(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=45, unique=False)
    json_dict = models.JSONField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Filter, self).save(*args, **kwargs)


class Panel(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=45, unique=False)
    json_dict = models.JSONField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Panel, self).save(*args, **kwargs)


class Batch(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=70)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Batch, self).save(*args, **kwargs)


class Entry(models.Model):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=45)
    sample_id = models.IntegerField()
    read_cutoff = models.IntegerField(default=30)
    abd_cutoff = models.FloatField(default=0)
    fscore_cutoff = models.IntegerField(default=20)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    batch = models.ForeignKey(Batch, default=None, blank=True, null=True, on_delete=models.SET_NULL)
    batch_param_active = models.BooleanField(default=True)
    filters = models.ManyToManyField(Filter, default=None, blank=True)
    panels = models.ManyToManyField(Panel, default=None, blank=True)


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Entry, self).save(*args, **kwargs)