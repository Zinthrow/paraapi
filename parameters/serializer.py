from .models import Batch, Entry, Filter, Panel
from rest_framework import serializers
from rest_framework import permissions



class FilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Filter
        fields = ['name', 'json_dict', 'updated_at', 'created_at']


class PanelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Panel
        fields = ['name', 'json_dict', 'updated_at', 'created_at']

class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ['name', 'id', 'sample_id', 'read_cutoff', 'abd_cutoff', 'fscore_cutoff', 'batch_id', 'filters', 'panels']

class ConfigSerializer(EntrySerializer):
    filters = FilterSerializer(read_only=True, many=True)
    panels = PanelSerializer(read_only=True, many=True)
    class Meta:
        model = Entry
        fields = ['name', 'id','sample_id', 'read_cutoff', 'abd_cutoff', 'fscore_cutoff', 'batch_id', 'filters', 'panels']

    def to_representation(self, instance):
        representation = super(ConfigSerializer, self).to_representation(instance)
        if representation['filters'] is not None:
            concat_filter = {}
            filter_names = []
            panel_names = []
            for filt in representation['filters']:
                filter_names.append(filt['name'])
                filt = filt['json_dict']
                for taxa in filt:
                    if taxa not in concat_filter:
                        concat_filter[taxa] = filt[taxa]
                    else:
                        concat_filter[taxa].extend(filt[taxa])
            representation['filters'] = concat_filter
            representation['filter_list'] = filter_names

        if representation['panels'] is not None:
            concat_panel = {}
            concat_names = []
            for pan in representation['panels']:
                concat_names.append(pan['name'])
                pan = pan['json_dict']
                for taxa in pan:
                    if taxa not in concat_panel:
                        concat_panel[taxa] = pan[taxa]
                    else:
                        concat_filter[taxa].extend(pan[taxa])
            representation['panels'] = concat_panel
            representation['panel_list'] = concat_names

        return representation

            

class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batch
        fields = ['name', 'updated_at', 'created_at']