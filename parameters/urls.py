from . import views

from django.urls import include, path
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'entry', views.EntryViewSet)
router.register(r'batch', views.BatchViewSet)
router.register(r'filter', views.FilterViewSet)
router.register(r'panel', views.PanelViewSet)
router.register(r'config', views.ConfigViewSet)

urlpatterns = [
    path('parameters/api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]