from django.contrib import admin

from .models import Panel, Filter, Batch, Entry
# Register your models here.


class PanelAdmin(admin.ModelAdmin):
    # ...
    list_display = ['name', 'updated_at', 'created_at']
    search_fields = ['name']

class FilterAdmin(admin.ModelAdmin):
    # ...
    list_display = ['name', 'updated_at', 'created_at', 'json_dict']
    search_fields = ['name']

class EntryInline(admin.TabularInline):
    model = Entry

class BatchAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'is_active', 'updated_at', 'created_at')
    search_fields = ['name']
    inlines = [
        EntryInline
    ]

class EntryAdmin(admin.ModelAdmin):
    list_display = ('name','id', 'is_active', 'batch', 'sample_id', 'updated_at', 'created_at')
    search_fields = ['name', 'batch__name']


admin.site.register(Entry, EntryAdmin)
admin.site.register(Batch, BatchAdmin)
admin.site.register(Filter, FilterAdmin)
admin.site.register(Panel, PanelAdmin)